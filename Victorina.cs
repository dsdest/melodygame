﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Win32;
using System;
using System.Windows.Forms;

namespace MelodyGame
{
    static class Victorina
    {
        public static List<string> SongList = new List<string>();
        public static int GameDuration = 60;
        public static int MusicDiration = 10;
        public static bool RandomStart = false;
        public static string LastFolder = "";
        public static bool AllDir = false;

        public static void ReadMusic()
        {
            try
            {
                string[] MusicList = Directory.GetFiles(LastFolder, "*.mp3", AllDir ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
                SongList.Clear();
                SongList.AddRange(MusicList);
            }
            catch
            {

            }
            
        }

        const string RegKeyName = @"Software\Cat Industries\MelodyGame";

        public static void WriteParams()
        {
            RegistryKey key = null;
            try
            {
                key = Registry.CurrentUser.CreateSubKey(RegKeyName);
                if (key == null) return;
                key.SetValue("LastFolder", LastFolder);
                key.SetValue("RandomStart", RandomStart);
                key.SetValue("GameDuration", GameDuration);
                key.SetValue("MusicDiration", MusicDiration);
                key.SetValue("AllDir", AllDir);
            }
            finally
            {
                if (key != null) key.Close();
            }
        }
        public static void ReadParams()
        {
            RegistryKey key = null;
            try
            {
                key = Registry.CurrentUser.OpenSubKey(RegKeyName);
                if (key!=null)
                {
                    LastFolder = (string)key.GetValue("LastFolder");
                    RandomStart = Convert.ToBoolean(key.GetValue("RandomStart"));
                    GameDuration = (int)key.GetValue("GameDuration");
                    MusicDiration = (int)key.GetValue("MusicDiration");
                    AllDir = Convert.ToBoolean(key.GetValue("AllDir"));
                }
            }
            finally
            {
                if (key != null) key.Close();
            }
        }
    }
}
