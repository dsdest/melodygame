﻿using System;
using System.IO;
using System.Windows.Forms;

namespace MelodyGame
{
    public partial class FParams : Form
    {
        public FParams()
        {
            InitializeComponent();
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            Victorina.AllDir = CheckAllDir.Checked;
            Victorina.GameDuration = Convert.ToInt32(CBGameDuration.Text);
            Victorina.MusicDiration = Convert.ToInt32(CBMusicDiration.Text);
            Victorina.RandomStart = CBRandomStart.Checked;
            Victorina.WriteParams();
            this.Hide();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            SetParams();
            this.Hide();
        }

        private void SetParams()
        {
            CheckAllDir.Checked = Victorina.AllDir;
            CBGameDuration.Text = Victorina.GameDuration.ToString();
            CBMusicDiration.Text = Victorina.MusicDiration.ToString();
            CBRandomStart.Checked = Victorina.RandomStart;
        }

        private void BtnSelectDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog()==DialogResult.OK)
            {
                string[] MusicList = Directory.GetFiles(dialog.SelectedPath, "*.mp3", CheckAllDir.Checked ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
                SongList.Items.Clear();
                SongList.Items.AddRange(MusicList);
                Victorina.SongList.Clear();
                Victorina.SongList.AddRange(MusicList);
                Victorina.LastFolder = dialog.SelectedPath;
            }
        }

        private void FParams_Load(object sender, EventArgs e)
        {
            SetParams();
            SongList.Items.Clear();
            SongList.Items.AddRange(Victorina.SongList.ToArray());

        }
    }
}
