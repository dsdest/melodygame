﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MelodyGame
{
    public partial class FMain : Form
    {
        private readonly FParams fParams = new FParams();
        private readonly FGame fGame = new FGame();
        public FMain()
        {
            InitializeComponent();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void BtnParams_Click(object sender, EventArgs e)
        {
            fParams.ShowDialog();
        }

        private void BtnPlay_Click(object sender, EventArgs e)
        { 
            fGame.ShowDialog();
        }

        private void FMain_Load(object sender, EventArgs e)
        {
            Victorina.ReadParams();
            Victorina.ReadMusic();
        }
    }
}
